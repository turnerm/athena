#include "../TimeBurner.h"
#include "../RandomErrorAlg.h"
#include "../EndOfEventROIConfirmerAlg.h"
#include "../EndOfEventPrescaleCheckAlg.h"
#include "../TrigEventInfoRecorderAlg.h"
#include "../L1CorrelationAlg.h"

DECLARE_COMPONENT( TimeBurner )
DECLARE_COMPONENT( RandomErrorAlg )
DECLARE_COMPONENT( EndOfEventROIConfirmerAlg )
DECLARE_COMPONENT( EndOfEventPrescaleCheckAlg )
DECLARE_COMPONENT( TrigEventInfoRecorderAlg )
DECLARE_COMPONENT( L1CorrelationAlg )
